//
//  ProgressCell.h
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 15/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIProgressView * progressBar;


@end
