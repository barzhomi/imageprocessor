//
//  ViewController.m
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 15/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import "ViewController.h"
#import "UIImageView+NetworkDownloading.h"
#import "ResultCell.h"
#import "ProgressCell.h"
#import "AppDelegate.h"
#import "HistoryItem.h"
#import "IPImageOperations.h"

@interface ViewController ()

@property (nonatomic,weak) IBOutlet UIImageView * sourceImageView;
@property (nonatomic,weak) IBOutlet UITableView * historyTableView;
@property (nonatomic) NSMutableArray * historyItems;
@property (nonatomic) NSMutableDictionary * imageOperations;

@property (nonatomic) UIActionSheet * imagePickerActionSheet;
@property (nonatomic) UIActionSheet * resultImageActionSheet;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _imagePickerActionSheet = [[UIActionSheet alloc] initWithTitle:@"Which source would you like to chose for taking a photo?"
                                                          delegate:self
                                                 cancelButtonTitle:@"Cancel"
                                            destructiveButtonTitle:nil
                                                 otherButtonTitles:@"Camera", @"Photo Library", @"URL", nil];
    _resultImageActionSheet = [[UIActionSheet alloc] initWithTitle:@"What would you like to do with that photo?"
                                 delegate:self
                        cancelButtonTitle:@"Cancel"
                   destructiveButtonTitle:@"Delete"
                        otherButtonTitles:@"Set as a Source", @"Save in Photo Library", nil];
    
    [self reloadHistoryItems];
    [_historyTableView reloadData];
}

-(void)reloadHistoryItems
{
    AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"HistoryItem" inManagedObjectContext:[delegate managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    NSArray *items = [[delegate managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    if(error){
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    _historyItems = [NSMutableArray arrayWithArray:items];
}

-(IBAction)imagePickerButtonClicked:(id)sender{
    [_imagePickerActionSheet showInView:self.view];
}


-(void)createNewHistoryItemWithImageOperation:(UIImageOperationType)imageOperationType{
    if(self.sourceImageView.image){
        AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
        
        NSEntityDescription * historyItemEntity = [NSEntityDescription entityForName:@"HistoryItem" inManagedObjectContext:[delegate managedObjectContext]];
        HistoryItem * historyItem = (HistoryItem*)[[NSManagedObject alloc] initWithEntity:historyItemEntity insertIntoManagedObjectContext:nil];
        
        historyItem.date = [NSDate date];
        
        [_historyItems insertObject:historyItem atIndex:0];
        
        IPImageOperation *imageOperation = nil;
        
        switch (imageOperationType) {
            case UIImageMirrorOperation:
                imageOperation = [[IPMirrorImageOperation alloc] initWithSourceImage:self.sourceImageView.image];
                break;
            case UIImageRotateOperation:
                imageOperation = [[IPRotateImageOperation alloc] initWithSourceImage:self.sourceImageView.image];
                break;
            case UIImageGrayscaleOperation:
                imageOperation = [[IPGrayscaleImageOperation alloc] initWithSourceImage:self.sourceImageView.image];
                break;
            default:
                break;
        }
        
        [imageOperation setImageOperationCompletionBlock:^(UIImage *image , NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
                [self.historyTableView beginUpdates];
                NSUInteger index = [_historyItems indexOfObject:historyItem];
                NSIndexPath * historyItemIndexPath = [NSIndexPath indexPathForRow:index inSection:0];

                if(error){
                    [_historyItems removeObjectAtIndex:index];
                    [self.historyTableView deleteRowsAtIndexPaths:@[historyItemIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                } else if(image){
                    [historyItem setImage:image];
                    [[delegate managedObjectContext] insertObject:historyItem];
                    [self.historyTableView reloadRowsAtIndexPaths:@[historyItemIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                }
                [delegate saveContext];
                [self.historyTableView endUpdates];
            });
        }];
        
        [historyItem setImageOperation:imageOperation];
        
        [NSThread detachNewThreadSelector:@selector(start) toTarget:imageOperation withObject:nil];
        
        [_historyTableView reloadData];
    }
}

-(IBAction)rotateButtonClicked:(id)sender{
    [self createNewHistoryItemWithImageOperation:UIImageRotateOperation];
}

-(IBAction)invertButtonClicked:(id)sender{
    [self createNewHistoryItemWithImageOperation:UIImageGrayscaleOperation];
}

-(IBAction)mirrorImageButtonClicked:(id)sender{
    [self createNewHistoryItemWithImageOperation:UIImageMirrorOperation];
}

#pragma mark - ActionSheet delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if([_imagePickerActionSheet isEqual:actionSheet]){
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Camera"]) {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
        } else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Photo Library"]) {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        } else if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"URL"]){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Enter image URL address" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Download", nil];
            alert.alertViewStyle=UIAlertViewStylePlainTextInput;
            [alert show];
        }
    } else if([_resultImageActionSheet isEqual:actionSheet]){
        NSIndexPath * selectedRow = [self.historyTableView indexPathForSelectedRow];
        HistoryItem * historyItem = [self.historyItems objectAtIndex:selectedRow.row];
        [self.historyTableView deselectRowAtIndexPath:selectedRow animated:YES];
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Set as a Source"]) {
            [self.sourceImageView setImage:historyItem.image];
        } else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Save in Photo Library"]) {
            UIImageWriteToSavedPhotosAlbum(historyItem.image,nil,nil,nil);
        } else if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Delete"]){
            AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
            [self.historyTableView beginUpdates];
            [[delegate managedObjectContext] deleteObject:historyItem];
            [_historyItems removeObject:historyItem];
            [self.historyTableView deleteRowsAtIndexPaths:@[selectedRow] withRowAnimation:UITableViewRowAnimationFade];
            [delegate saveContext];
            [self.historyTableView endUpdates];
        }
    }
}

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

#pragma mark - AlertView delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Download"]) {
        UITextField * urlTextField = [alertView textFieldAtIndex:0];
        if(urlTextField && urlTextField.text){
            [_sourceImageView setImageWithURL:[NSURL URLWithString:urlTextField.text ]];
        }
    }
}

#pragma mark - ImagePicker delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
    
    [_sourceImageView setImage:image];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - TableView data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    HistoryItem * historyItem = [_historyItems objectAtIndex:indexPath.row];
    if(historyItem.image){
        return 160;
    }else {
        return 44;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _historyItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HistoryItem * historyItem = [_historyItems objectAtIndex:indexPath.row];
    
    if(historyItem.image){
        ResultCell *cell = nil;
        cell = [tableView dequeueReusableCellWithIdentifier:@"ResultCell" forIndexPath:indexPath];
        if (!cell) {
            cell = [[ResultCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ResultCell"];
        }
        cell.resultImage.image = historyItem.image;
        return cell;
    } else if(historyItem.imageOperation) {
        ProgressCell *cell = nil;
        cell = [tableView dequeueReusableCellWithIdentifier:@"ProgressCell" forIndexPath:indexPath];
        if (!cell) {
            cell = [[ProgressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ProgressCell"];
        }
        
       [cell.progressBar setProgress:historyItem.imageOperation.currentProgress animated:NO];
        
        [historyItem.imageOperation setImageOperationProgressBlock:^(NSUInteger progress, NSUInteger total) {
            [cell.progressBar setProgress:(float)progress/(float)total animated:YES];
        }];
        return cell;
    }
    return nil;
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HistoryItem * historyItem = [_historyItems objectAtIndex:indexPath.row];
    
    if(historyItem.image){
        return indexPath;
    } else {
        return nil;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_resultImageActionSheet showInView:self.view];
}
@end
