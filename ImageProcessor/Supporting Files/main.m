//
//  main.m
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 15/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
