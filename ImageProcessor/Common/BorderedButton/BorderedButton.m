//
//  BorderedButton.m
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 15/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import "BorderedButton.h"

@implementation BorderedButton

-(void)drawRect:(CGRect)rect{
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.layer.borderWidth = 1.0f;
}


@end
