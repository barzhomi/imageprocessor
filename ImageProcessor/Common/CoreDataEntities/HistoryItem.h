//
//  HistoryItem.h
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 19/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "IPImageOperations.h"


@interface HistoryItem : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSData * imageData;

@property (nonatomic) UIImage * image;
@property (nonatomic) IPImageOperation *imageOperation;

@end
