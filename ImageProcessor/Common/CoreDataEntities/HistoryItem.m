//
//  HistoryItem.m
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 19/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import "HistoryItem.h"


@implementation HistoryItem

@dynamic date;
@dynamic imageData;
@synthesize imageOperation;

-(void)setImage:(UIImage *)image{
    if(image){
        NSData * imageData = UIImagePNGRepresentation(image);
        if(![self.imageData isEqualToData:imageData]){
            self.imageData = imageData;
        }
    }
}

-(UIImage *)image{
    if(self.imageData){
        return [UIImage imageWithData:self.imageData];
    }
    return  nil;
}


@end
