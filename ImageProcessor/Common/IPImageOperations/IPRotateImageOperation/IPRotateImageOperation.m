//
//  IPRotateImageOperation.m
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 19/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import "IPRotateImageOperation.h"

@implementation IPRotateImageOperation

-(instancetype)initWithSourceImage:(UIImage *)sourceImage{
    self = [super initWithSourceImage:sourceImage];
    if(self){
        IPImageOperation * blockSelf = self;
        imageOperationBlock = ^UIImage * {
            return [blockSelf.sourceImage rotateImage];
        };
    }
    return self;
}

@end
