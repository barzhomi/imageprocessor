//
//  IPImageOperations.h
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 19/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#ifndef ImageProcessor_IPImageOperations_h
#define ImageProcessor_IPImageOperations_h

#import "IPRotateImageOperation.h"
#import "IPMirrorImageOperation.h"
#import "IPGrayscaleImageOperation.h"

#endif
