//
//  IPGrayscaleImageOperation.m
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 19/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import "IPGrayscaleImageOperation.h"

@implementation IPGrayscaleImageOperation


-(instancetype)initWithSourceImage:(UIImage *)sourceImage{
    self = [super initWithSourceImage:sourceImage];
    if(self){
        IPImageOperation * blockSelf = self;
        imageOperationBlock = ^UIImage * {
            return [blockSelf.sourceImage grayscaleImage];
        };
    }
    return self;
}


@end
