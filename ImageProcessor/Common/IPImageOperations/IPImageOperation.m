//
//  IPImageOperation.m
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 19/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import "IPImageOperation.h"

@implementation IPImageOperation

-(instancetype)initWithSourceImage:(UIImage *)sourceImage{
    self = [super init];
    if(self){
        self.sourceImage = sourceImage;
        _currentProgress = 0;
    }
    return  self;
}

-(void)main{
    NSUInteger randomOperationTime = arc4random_uniform(25) + 5;
    
    for(int i = 0; i < randomOperationTime; i++){
        _currentProgress = (float)i/(float)(randomOperationTime-1) ;
        if(self.imageOperationProgressBlock){
            dispatch_async(dispatch_get_main_queue(), ^{
                self.imageOperationProgressBlock(i, randomOperationTime-1);
            });}
        [NSThread sleepForTimeInterval:1.0];
    }
    
    UIImage *newImage = [[UIImage alloc] init];
    
    if(imageOperationBlock){
       newImage = imageOperationBlock();
    }

    if(self.imageOperationCompletionBlock){
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageOperationCompletionBlock(newImage, nil);
        });
    }
}




@end
