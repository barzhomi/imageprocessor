//
//  IPImageOperation.h
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 19/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIImage+ImageOperations.h"

@interface IPImageOperation : NSOperation
{
@protected
    UIImage * (^imageOperationBlock)();
}
@property(atomic,readonly) float currentProgress;
@property(atomic,copy) void (^imageOperationProgressBlock)(NSUInteger progress, NSUInteger total);
@property(copy) void (^imageOperationCompletionBlock)(UIImage *image, NSError *error);

@property(copy) UIImage * sourceImage;

-(instancetype)initWithSourceImage:(UIImage*)sourceImage;

@end
