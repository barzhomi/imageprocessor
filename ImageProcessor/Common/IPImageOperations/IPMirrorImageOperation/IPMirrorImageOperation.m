//
//  IPMirrorImageOperation.m
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 19/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import "IPMirrorImageOperation.h"


@implementation IPMirrorImageOperation

-(instancetype)initWithSourceImage:(UIImage *)sourceImage{
    self = [super initWithSourceImage:sourceImage];
    if(self){
        IPImageOperation * blockSelf = self;
        imageOperationBlock = ^UIImage * {
            return [blockSelf.sourceImage mirrorImage];
        };
    }
    return self;
}

@end
