//
//  UIImage+ImageOperations.m
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 10/20/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import "UIImage+ImageOperations.h"

@implementation UIImage (ImageOperations)

-(UIImage*)rotateImage {
    CGSize imgSize = [self size];
    
    UIGraphicsBeginImageContext(CGSizeMake(imgSize.height,imgSize.width));
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextRotateCTM(context, M_PI_2);
    CGContextTranslateCTM(context, 0, -imgSize.height);
    [self drawInRect:CGRectMake(0, 0, imgSize.width, imgSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(UIImage*)mirrorImage{
    CGAffineTransform  transfrom = CGAffineTransformIdentity;
    
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    
    transfrom = CGAffineTransformMakeTranslation(rect.size.width, 0.0);
    transfrom = CGAffineTransformScale(transfrom, -1.0, 1.0);
    
    UIGraphicsBeginImageContext(CGSizeMake(rect.size.width, rect.size.height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, 0.0, -rect.size.height);
    
    
    CGContextConcatCTM(context, transfrom);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), rect, [self CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(UIImage*)grayscaleImage{
    CGRect imageRect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGContextRef context = CGBitmapContextCreate(nil, self.size.width, self.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    
    CGContextDrawImage(context, imageRect, [self CGImage]);
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    return newImage;   
}


@end
