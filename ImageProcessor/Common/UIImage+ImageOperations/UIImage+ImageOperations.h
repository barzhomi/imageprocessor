//
//  UIImage+ImageOperations.h
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 10/20/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, UIImageOperationType) {
    UIImageRotateOperation = 0,
    UIImageMirrorOperation = 1,
    UIImageGrayscaleOperation = 2,
};

@interface UIImage (ImageOperations)

-(UIImage*)rotateImage;
-(UIImage*)mirrorImage;
-(UIImage*)grayscaleImage;
-(UIImage*)cropedToCenterImage;

@end
