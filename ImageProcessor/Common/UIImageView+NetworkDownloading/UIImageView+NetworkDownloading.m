//
//  UIImageView+NetworkDownloading.m
//  ImageProcessor
//
//  Created by Bazhen Rzheutskiy on 15/10/14.
//  Copyright (c) 2014 TestOrganization. All rights reserved.
//

#import "UIImageView+NetworkDownloading.h"

@implementation UIImageView (NetworkDownloading)

-(void)setImageWithURL:(NSURL*)imageURL{
    UIActivityIndicatorView * indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.color = [UIColor blueColor];
    CGRect viewFrame = self.frame;
    CGFloat indicatorWidth = indicator.frame.size.width;
    CGPoint indicatorCenteredOrigin = CGPointMake((viewFrame.size.width - indicatorWidth)/2, (viewFrame.size.height - indicatorWidth)/2);
    indicator.frame = CGRectMake(indicatorCenteredOrigin.x, indicatorCenteredOrigin.y, indicator.frame.size.width, indicator.frame.size.height);
    [self addSubview:indicator];
    [indicator startAnimating];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        NSError * error = nil;
        NSData * imageData = [NSData dataWithContentsOfURL:imageURL options:NSDataReadingUncached error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(!error){
                self.image = [UIImage imageWithData:imageData];
            }
            [indicator stopAnimating];
            [indicator removeFromSuperview];
        });
    });
}
@end
